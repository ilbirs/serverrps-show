package com.example.serverrps;


import com.example.serverrps.Game.*;
import com.example.serverrps.Token.*;
import com.example.serverrps.move.Move;
import com.example.serverrps.status.Status;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("integrationtest")
class ServerRpsApplicationTests {

    @LocalServerPort
    private int port;
    private TestRestTemplate testRestTemplate;


    @Autowired
    GameRepository gameRepository;
    @Autowired
    TokenRepository tokenRepository;

    @Autowired
    GameService gameService;
    @Autowired
    TokenService tokenService;

    @Autowired
    TokenController tokenController;

    @Autowired
    GameController gameController;


    @BeforeEach
    void setup() {
        testRestTemplate = new TestRestTemplate();
        gameRepository.deleteAll();

    }

    @Test
    void test_get_token() {
        testRestTemplate = new TestRestTemplate().withBasicAuth("admin","password");
        //Given
        String token = tokenController.getNyToken();

        //Then
        assertNotNull(token);


    }

//    @Test
//    void test_get_new_token() {
//        //Given
//
//        //when
//        String token = testRestTemplate.getForObject(testUrl("/auth/token"), String.class);
//        System.out.println("DemoApplicationTests.test " + token);
//
//        //then
//
//        assertNotNull(token);
//    }

    private String testUrl(String path) {
        return "http://localhost:" + port + path;
    }

    @Test
    void test_get_addNameToToken() {
        //Given
        String token = tokenController.getNyToken();
        //when
        Token tokenControllerTest = tokenController.create("Diako", token);
        //then
        assertNotNull(token);
        assertEquals("Diako", tokenControllerTest.getName());

    }

    @Test
    void test_create_game() {
        //Given
        String token = tokenController.getNyToken();
        Token tokenName = tokenController.create("Diako", token);

        //when
        Game game = gameController.createGame(tokenName.getId());
        game.getPlayer();


        //then
        assertEquals("Diako", game.getPlayer());
        assertEquals(Status.OPEN, game.getStatus());


    }

    @Test
    void test_join_game() {
//        Given
        String token = tokenController.getNyToken();
        Token tokenName = tokenController.create("Diako", token);
        Game game = gameController.createGame(tokenName.getId());

        String tokenOpponent = tokenController.getNyToken();
        Token tokenOpponentName = tokenController.create("Rinat", tokenOpponent);

        //when

        Game joinGame = gameController.join(game.getId(), tokenOpponentName.getId());


        assertEquals("Diako", joinGame.getOpponent());
        assertEquals("Rinat", joinGame.getPlayer());

        //then

    }


    @Test
    void test_open_games() {
        //Given
        String token = tokenController.getNyToken();
        Token tokenName = tokenController.create("Diako", token);
        gameController.createGame(tokenName.getId());
        //when

        List<Game> openGames = gameController.openGames(token);

        //then
        assertNotNull(openGames);
        assertEquals(Status.OPEN, openGames.get(0).getStatus());

    }

    @Test
    void test_move() {
        //Given
        String token = tokenController.getNyToken();
        Token tokenName = tokenController.create("Diako", token);
        gameController.createGame(tokenName.getId());
        //when

        Game game = gameController.getMove(token, "sax");

        //then
        assertEquals("Diako", game.getPlayer());
        assertEquals(Move.SCISSORS, game.getPlayerMove());

    }

    @Test
    void test_status() {
        //Given
        String token = tokenController.getNyToken();
        Token tokenName = tokenController.create("Diako", token);
        gameController.createGame(tokenName.getId());
        Game game = gameController.getMove(token, "sax");

        String tokenOpponent = tokenController.getNyToken();
        Token tokenOpponentName = tokenController.create("Rinat", tokenOpponent);

        gameController.join(game.getId(), tokenOpponentName.getId());
        gameController.getMove(tokenOpponent, "rock");

        //when
        Game gameStats = gameController.activeGames(token);
        gameStats.getOpponent();

        //then
        assertNotNull("Diako", gameStats.getPlayer());
        assertNotNull(gameStats.getPlayerMove());
        assertNotNull(gameStats.getStatus());
        assertNotNull(gameStats.getOpponent());
        assertNotNull(gameStats.getOpponentMove());

        assertEquals("Diako", gameStats.getPlayer());
        assertEquals(Move.SCISSORS, gameStats.getPlayerMove());
        assertEquals(Status.LOSE, gameStats.getStatus());
        assertEquals("Rinat", gameStats.getOpponent());
        assertEquals(Move.ROCK, gameStats.getOpponentMove());


    }

    @Test
    void test_game_info() {
        //Given
        String token = tokenController.getNyToken();
        Token tokenName = tokenController.create("Diako", token);
        gameController.createGame(tokenName.getId());
        Game game = gameController.getMove(token, "sax");

        String tokenOpponent = tokenController.getNyToken();
        Token tokenOpponentName = tokenController.create("Rinat", tokenOpponent);

        gameController.join(game.getId(), tokenOpponentName.getId());
        gameController.getMove(tokenOpponent, "rock");

        //when
        Game gameStats = gameController.gameInfo(token, game.getId());


//        then
        assertEquals("Diako", gameStats.getPlayer());
        assertEquals(Move.SCISSORS, gameStats.getPlayerMove());
        assertEquals(Status.LOSE, gameStats.getStatus());
        assertEquals("Rinat", gameStats.getOpponent());
        assertEquals(Move.ROCK, gameStats.getOpponentMove());

    }

    @Test
    void test_move_choise() {
        Move sax = gameService.moveChoice("sax");
        Move sten = gameService.moveChoice("sten");
        Move papper = gameService.moveChoice("papper");


        assertEquals(Move.SCISSORS, sax);
        assertEquals(Move.ROCK, sten);
        assertEquals(Move.PAPER, papper);


    }


    @Test
    void test_game_status_game_player() {
        Status testOne = gameService.gamePlayer(null, Move.SCISSORS, Move.SCISSORS);
        Status testTwo = gameService.gamePlayer(null, Move.SCISSORS, Move.ROCK);
        Status testThree = gameService.gamePlayer(null, Move.ROCK, Move.SCISSORS);
        ;

        assertEquals(Status.DRAW,testOne);
        assertEquals(Status.LOSE, testTwo);
        assertEquals(Status.WIN, testThree);


    }

    @Test
    void test_game_status_game_opponenet() {
        Status testOne = gameService.gameOponent(null, Move.SCISSORS, Move.SCISSORS);
        Status testTwo = gameService.gameOponent(null, Move.SCISSORS, Move.ROCK);
        Status testThree = gameService.gameOponent(null, Move.ROCK, Move.SCISSORS);

        assertEquals(Status.DRAW,testOne);
        assertEquals(Status.LOSE, testTwo);
        assertEquals(Status.WIN, testThree);


    }




}
