package com.example.serverrps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServerRpsApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServerRpsApplication.class, args);
    }

}
