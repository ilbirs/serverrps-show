package com.example.serverrps.Token;

import com.example.serverrps.Game.GameEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "token")
public class TokenEntity {
    @Id
    String id;
    @Column(name="name")
    String name;
   /* @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="name_id",referencedColumnName = "id")
    NameEntity name;*/

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="owned_game_id",referencedColumnName = "id")
    GameEntity ownedGame;


    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="join_game_id",referencedColumnName = "id")
    GameEntity joinGame;

    public TokenEntity(String id) {
        this.id = id;
    }
}
