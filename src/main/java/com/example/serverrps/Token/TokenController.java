package com.example.serverrps.Token;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@AllArgsConstructor
@JsonIgnoreProperties
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class TokenController {
    TokenService tokenService;

    @GetMapping("/auth/token")
    public String getNyToken(){
        return tokenService.creatToken().getId();
    }

    @PostMapping("/user/name")
    public Token create(@RequestBody String name,
                                        @RequestHeader(value = "token") String tokenId) {
        return tokenService.createName(tokenId,name).map(this::toToken).orElse(null);

    }

    @DeleteMapping("/user/delete/{token}")
    void deleteToken(@PathVariable(value = "token") String token) {
        tokenService.deleteToken(token);
    }


    public Token toToken(TokenEntity tokenEntity) {
        return new Token(
                tokenEntity.getId(),
                tokenEntity.getName(),
                tokenEntity.getOwnedGame() !=null?tokenEntity.getOwnedGame().getId():null,
                tokenEntity.getJoinGame() !=null?tokenEntity.getJoinGame().getId():null
        );
    }
}
