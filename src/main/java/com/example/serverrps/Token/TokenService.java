package com.example.serverrps.Token;


import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class TokenService {

    TokenRepository tokenRepository;

    public TokenService(TokenRepository tokenRepository) {
        this.tokenRepository = tokenRepository;
    }

    public TokenEntity creatToken() {
        TokenEntity token = new TokenEntity(UUID.randomUUID().toString());
        tokenRepository.save(token);
        return token;
    }

    public TokenEntity getTokenId(String tokenId){
        return tokenRepository.getOne(tokenId);
    }

    public Optional<TokenEntity> getTokenByTokenString(String tokenId){
        return tokenRepository.findById(tokenId);
    }

    public Optional<TokenEntity> createName(String token, String name) {
      return getTokenByTokenString(token)
              .map(a->{
                  a.setName(name);
                  tokenRepository.save(a);
                  return a;
              });

    }

    public void deleteToken(String id){
        tokenRepository.deleteById(id);
    }

}
