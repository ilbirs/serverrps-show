package com.example.serverrps.Token;

import lombok.Value;

@Value
public class Token {
    String id;
    String name;
    String ownedGame;
    String joinGame;
}
