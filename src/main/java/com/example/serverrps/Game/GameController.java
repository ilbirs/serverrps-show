package com.example.serverrps.Game;

import com.example.serverrps.Token.TokenEntity;
import com.example.serverrps.Token.TokenService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class GameController {


    TokenService tokenService;
    GameService gameService;

    public GameController(TokenService tokenService, GameService gameService) {
        this.tokenService = tokenService;
        this.gameService = gameService;
    }


 @GetMapping("/games/{id}")
    public Game gameInfo(@RequestHeader(value = "token") String tokenId,
                                 @PathVariable(value = "id") String gameId) {
     TokenEntity token = tokenService.getTokenByTokenString(tokenId).orElse(null);
        return gameService.getGameById(gameId,token)
                .map(gameEntity1 -> toGame(gameEntity1,token)).orElse(null);
    }




    @GetMapping("/games/move")
    public Game getMove(@RequestHeader(value = "token") String tokenId,
                                   @RequestParam(value = "move") String move) {
        TokenEntity token = tokenService.getTokenByTokenString(tokenId).orElse(null);
        return gameService.moveLogic(tokenId, move)
                .map(gameEntity1 -> toGame(gameEntity1, token)).orElse(null);

    }


    @GetMapping("/games/join/{gameId}")
    public Game join(@PathVariable(value = "gameId") String gameId,
                             @RequestHeader(value = "token") String tokenId) {
        TokenEntity token = tokenService.getTokenByTokenString(tokenId).orElse(null);
        return gameService.joinaGame(gameId, token)
                .map(gameEntity1 -> toGame(gameEntity1, token)).orElse(null);
    }

    @GetMapping("/games/status")
    public Game activeGames(@RequestHeader(value = "token") String tokenId) {
        TokenEntity token = tokenService.getTokenByTokenString(tokenId).orElse(null);
        return gameService.getGameStatus(tokenId)
                .map(gameEntity1 -> toGame(gameEntity1,token)).orElse(null);

    }



    @GetMapping("/games/start")
    public Game createGame(@RequestHeader(value = "token") String tokenId) {
        TokenEntity token = tokenService.getTokenByTokenString(tokenId).orElse(null);
        return gameService.createStart(tokenId)
                .map(gameEntity -> toGame(gameEntity,token)).orElse(null);


    }

   @GetMapping("/games")
    public List<Game> openGames(@RequestHeader(value = "token")  String tokenId) {
       TokenEntity token = tokenService.getTokenId(tokenId);
        return gameService.allOpenGame()
                .map(gameEntity1 -> toGame(gameEntity1,token)).collect(Collectors.toList());
    }





    public Game toGame(GameEntity gameEntity, TokenEntity token) {
        if (token.getJoinGame() == null){
            return new Game(gameEntity.getId(),
                    gameEntity.getPlayer() != null?gameEntity.getPlayer().getName(): null,
                    gameEntity.getPlayerMove()!=null?gameEntity.getPlayerMove():null,
                    gameEntity.getStatus(),//win eller lose
                    gameEntity.getOpponent()!= null?gameEntity.getOpponent().getName(): null,
                    gameEntity.getOpponentMove()!=null?gameEntity.getOpponentMove():null
            );}
        return new Game(gameEntity.getId(),
                gameEntity.getOpponent()!= null?gameEntity.getOpponent().getName(): null,
                gameEntity.getOpponentMove()!=null?gameEntity.getOpponentMove():null,
                gameEntity.getStatus(),
                gameEntity.getPlayer()!= null?gameEntity.getPlayer().getName(): null,
                gameEntity.getPlayerMove()!=null?gameEntity.getPlayerMove():null
        );

    }
}

