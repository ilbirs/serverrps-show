package com.example.serverrps.Game;

import com.example.serverrps.move.Move;
import com.example.serverrps.status.Status;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class Game {
    String id;
    String player;
    Move playerMove;
    Status status;
    String opponent;
    Move opponentMove;


}
