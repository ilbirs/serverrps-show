package com.example.serverrps.Game;


import com.example.serverrps.Token.TokenEntity;
import com.example.serverrps.Token.TokenRepository;
import com.example.serverrps.exception.NotActualException;
import com.example.serverrps.move.Move;
import com.example.serverrps.status.Status;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

@Service
public class GameService {

    GameRepository gameRepository;
    TokenRepository tokenRepository;

    public GameService(GameRepository gameRepository, TokenRepository tokenRepository) {
        this.gameRepository = gameRepository;
        this.tokenRepository = tokenRepository;
    }

    public Optional<GameEntity> joinaGame(String id, TokenEntity token) {
        GameEntity game = gameRepository.getOne(id);
        if(game.getStatus().equals(Status.ACTIVE))
            throw new NotActualException();
        game.setStatus(Status.ACTIVE);
        game.setOpponent(token);
        token.setJoinGame(game);
        tokenRepository.save(token);
        gameRepository.save(game);
        return Optional.of(game);

    }

    public Optional<GameEntity> getGameById(String gameId, TokenEntity tokenById) {
        return gameRepository.findById(gameId)
                .map(gameEntity->{
                    if (tokenById.getOwnedGame() != null)
                        gameEntity.setStatus(gamePlayer(gameEntity,gameEntity.getPlayerMove(), gameEntity.getOpponentMove()));
                    if (tokenById.getJoinGame() != null)
                        gameEntity.setStatus(gameOponent(gameEntity,gameEntity.getOpponentMove(), gameEntity.getPlayerMove()));
                    return gameEntity;
                });

    }

    public Optional<TokenEntity> getTokenByTokenString(String id){
        return tokenRepository.findById(id);
    }

    public Optional<GameEntity> createStart(String token) {
        TokenEntity tokenEntity = getTokenByTokenString(token).orElse(null);
        GameEntity gameEntity = new GameEntity(UUID.randomUUID().toString());
        gameEntity.setPlayer(tokenEntity);
        gameEntity.setStatus(Status.OPEN);
        assert tokenEntity != null;
        tokenEntity.setOwnedGame(gameEntity);
        tokenRepository.save(tokenEntity);
        gameRepository.save(gameEntity);
        return Optional.of(gameEntity);

    }

    public Stream<GameEntity> allOpenGame() {
        return gameRepository.findAll().stream().filter(a -> a.getStatus().equals(Status.OPEN));
    }


    public Optional<GameEntity> getGameStatus(String id) {
        TokenEntity tokenById = tokenRepository.findById(id).orElseThrow(NullPointerException::new);
        return sortById(tokenById.getId())
                .map(gameEntity->{
                    if (tokenById.getOwnedGame() != null)
                        gameEntity.setStatus(gamePlayer(gameEntity,gameEntity.getPlayerMove(), gameEntity.getOpponentMove()));
                    if (tokenById.getJoinGame() != null)
                        gameEntity.setStatus(gameOponent(gameEntity,gameEntity.getOpponentMove(), gameEntity.getPlayerMove()));
                    return gameEntity;
                });
    }

    public Optional<GameEntity> sortById(String id){
        TokenEntity token = tokenRepository.findById(id).orElse(null);
        GameEntity gameEntity = gameRepository.findById(token.getOwnedGame()!=null?
                token.getOwnedGame().getId():
                token.getJoinGame().getId())
                .orElseThrow(NullPointerException::new);
        return Optional.of(gameEntity);
    }

    public Optional<GameEntity> moveLogic(String id, String move) {
        TokenEntity token = tokenRepository.findById(id).orElseThrow(NullPointerException::new);
        return sortById(token.getId())
                .map(gameEntity -> {
                   if (token.getOwnedGame()==gameEntity.getPlayer().getOwnedGame()){
                       setMovePlayer(gameEntity, move);
                   }else {
                       setMoveOpponenet(gameEntity, move);
                   }
                    gameRepository.save(gameEntity);
                    return gameEntity;
                });

    }

    public void setMoveOpponenet (GameEntity gameEntity, String move ) {
        gameEntity.setOpponentMove(moveChoice(move));
        gameEntity.setStatus(gameOponent(gameEntity,gameEntity.getOpponentMove(), gameEntity.getPlayerMove()));
    };
    public void setMovePlayer(GameEntity gameEntity, String move) {
        gameEntity.setPlayerMove(moveChoice(move));
        gameEntity.setStatus(gamePlayer(gameEntity,gameEntity.getPlayerMove(), gameEntity.getOpponentMove()));
    };

    public Status gamePlayer(GameEntity gameEntity, Move playerMove, Move opponentMove) {
        String move = playerMove + " " + opponentMove;
        return switch (move) {
            case "SCISSORS SCISSORS", "ROCK ROCK", "PAPER PAPER" -> Status.DRAW;
            case "SCISSORS PAPER", "ROCK SCISSORS", "PAPER ROCK" -> Status.WIN;
            case "PAPER SCISSORS", "SCISSORS ROCK", "ROCK PAPER" -> Status.LOSE;
            default -> gameEntity.getOpponent()==null?Status.OPEN:Status.ACTIVE;
        };
    }

    public Status gameOponent( GameEntity gameEntity, Move opponentMove, Move playerMove) {
        String move = opponentMove + " " + playerMove;
        return switch (move) {
            case "SCISSORS SCISSORS", "ROCK ROCK", "PAPER PAPER" -> Status.DRAW;
            case "SCISSORS PAPER", "ROCK SCISSORS", "PAPER ROCK" -> Status.WIN;
            case "PAPER SCISSORS", "SCISSORS ROCK", "ROCK PAPER" -> Status.LOSE;
            default ->  gameEntity.getOpponent()==null?Status.OPEN:Status.ACTIVE;
        };
    }

    public Move moveChoice(String move) {
        return switch (move) {
            case "sax", "SAX", "SCISSORS", "scissors" -> Move.SCISSORS;
            case "papper", "PAPPER", "PAPER", "paper" -> Move.PAPER;
            case "sten", "STEN", "ROCK", "rock" -> Move.ROCK;
            default -> throw new IllegalStateException("Unexpected value: " + move);
        };
    }




}

