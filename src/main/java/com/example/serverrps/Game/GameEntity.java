package com.example.serverrps.Game;


import com.example.serverrps.Token.TokenEntity;
import com.example.serverrps.move.Move;
import com.example.serverrps.status.Status;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "game")
public class GameEntity {
    @Id
    String id;

    @OneToOne(mappedBy = "ownedGame")
    TokenEntity player;

    @Enumerated(EnumType.STRING)
    @Column(name="payermove", columnDefinition = "ENUM('ROCK','PAPER','SCISSORS')")
    Move playerMove;

    @Enumerated(EnumType.STRING)
    @Column(name="status", columnDefinition = "ENUM('NONE','OPEN','ACTIVE','WIN','LOSE','DRAW')")
    Status status;

    @OneToOne(mappedBy = "joinGame")
    TokenEntity opponent;

    @Enumerated(EnumType.STRING)
    @Column(name="opponenetmove", columnDefinition = "ENUM('ROCK','PAPER','SCISSORS')")
    Move opponentMove;

    public GameEntity(String id) {
        this.id = id;
    }

}
